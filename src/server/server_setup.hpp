
#pragma once
#include <Crow/crow_all.h>
#include <string>
#include <stdexcept>
#include "config/config_loader.hpp"

namespace Server {
    class ServerConfigurator {
    public:
        ServerConfigurator(crow::SimpleApp& app, const Config::ConfigLoader& config) : app_(app), config_(config) {}

        void applySettings() {
            app_.bindaddr(config_.get<std::string>("server.bindaddr", "0.0.0.0"));
            app_.port(config_.get<int>("server.port", 8080));
            setLogLevel(config_.get<std::string>("logging.level", "info"));
        }

    private:
        void setLogLevel(const std::string& loglevel) {
            crow::LogLevel level;
            if (loglevel == "debug") {
                level = crow::LogLevel::Debug;
            } else if (loglevel == "info") {
                level = crow::LogLevel::Info;
            } else if (loglevel == "warning") {
                level = crow::LogLevel::Warning;
            } else if (loglevel == "error") {
                level = crow::LogLevel::Error;
            } else if (loglevel == "critical") {
                level = crow::LogLevel::Critical;
            } else {
                throw std::runtime_error("Unknown log level: " + loglevel);
            }
            app_.loglevel(level);
        }

        crow::SimpleApp& app_;
        const Config::ConfigLoader& config_;
    };
}
