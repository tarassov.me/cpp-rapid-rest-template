#include <Crow/crow_all.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace users {

    void init_routes(crow::SimpleApp& app) {
        CROW_ROUTE(app, "/users")
        ([]() {
            crow::json::wvalue response;
            response["status"] = "success";
            response["message"] = "List of users";
            response["data"] = crow::json::wvalue::list({ "user1", "user2", "user3" });
            return crow::response(response);
        });

        CROW_ROUTE(app, "/user/<string>")
        ([](const crow::request& req, std::string userId) {
            crow::json::wvalue response;
            response["status"] = "success";
            response["message"] = "User details";
            response["data"]["userId"] = userId;
            return crow::response(response);
        });

        CROW_ROUTE(app, "/user/<string>").methods("POST"_method)
        ([](const crow::request& req, std::string userId) {
            auto body = crow::json::load(req.body);
            crow::json::wvalue response;

            if (!body) {
                response["status"] = "error";
                response["message"] = "Invalid JSON format";
                return crow::response(400, response);
            }

            response["status"] = "success";
            response["message"] = "User created";
            response["data"]["userId"] = userId;
            response["data"]["name"] = body["name"].s();
            return crow::response(response);
        });
    }
}
