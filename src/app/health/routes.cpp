#include <Crow/crow_all.h>

namespace health {
    void init_routes(crow::SimpleApp& app) {
        CROW_ROUTE(app, "/health")
        ([](){
            return "Health check OK";
        });
    }
}
