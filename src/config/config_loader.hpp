
#pragma once
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <stdexcept>

namespace Config {
    class ConfigLoader {
    public:
        explicit ConfigLoader(const std::string& filename) {
            boost::property_tree::read_json(filename, config_);
        }

        template<typename T>
        T get(const std::string& key, const T& default_value) const {
            return config_.get<T>(key, default_value);
        }
    private:
        boost::property_tree::ptree config_;
    };
}
