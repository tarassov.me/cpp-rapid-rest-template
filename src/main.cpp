
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <Crow/crow_all.h>

#include "config/config_loader.hpp"
#include "server/server_setup.hpp"
#include "app/health/routes.cpp"
#include "app/users/routes.cpp"

class RouteInitializer {
public:
    static void initialize_routes(crow::SimpleApp& app) {
        health::init_routes(app);
        users::init_routes(app);
    }
};

int main() {
    crow::SimpleApp app;
    Config::ConfigLoader config("/app/config.json");
    Server::ServerConfigurator server_configurator(app, config);

    try {
        server_configurator.applySettings();
        RouteInitializer::initialize_routes(app);
        app.multithreaded().run();
    } catch (const std::exception& e) {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
