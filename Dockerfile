
# Stage 1: Build the application
FROM ubuntu:20.04 AS builder

# Install build tools and dependencies
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends g++ cmake make git libboost-all-dev libasio-dev libhttp-parser-dev

# Set work directory
WORKDIR /app

# Copy source files
COPY . .

# Build the application
RUN mkdir build && cd build &&     cmake .. &&     make -j$(nproc)

# Stage 2: Create final image with only the compiled application
FROM ubuntu:latest

# Copy the built application from the builder stage
COPY --from=builder /app/src/config.json /app/config.json
COPY --from=builder /app/build/app /usr/local/bin/app

# Expose port and run the application
EXPOSE 8080
CMD ["/usr/local/bin/app"]
